<?php
/**
 * Created by PhpStorm.
 * User: wtfssd
 * Date: 2017/6/5
 * Time: 22:21
 */

namespace app\Api\controller;


use controller\BasicAdmin;
use service\NodeService;
use service\ToolsService;
use think\Db;

class Index extends BasicAdmin
{
    public function index(){
        NodeService::applyAuthNode();
        $list = Db::name('SystemMenu')->where('status', '1')->order('sort asc,id asc')->select();
        $menus = $this->_filterMenu(ToolsService::arr2tree($list));
        $this->assign('title', '接口工具');
        $this->assign('menus', $menus);
        return view();
    }


    /**
     * 后台主菜单权限过滤
     * @param array $menus
     * @return array
     */
    private function _filterMenu($menus) {
        foreach ($menus as $key => &$menu) {
            if (!empty($menu['sub'])) {
                $menu['sub'] = $this->_filterMenu($menu['sub']);
            }
            if (!empty($menu['sub'])) {
                $menu['url'] = '#';
            } elseif (stripos($menu['url'], 'http') === 0) {
                continue;
            } elseif ($menu['url'] !== '#' && auth(join('/', array_slice(explode('/', $menu['url']), 0, 3)))) {
                $menu['url'] = url($menu['url']);
            } else {
                unset($menus[$key]);
            }
        }
        return $menus;
    }

}
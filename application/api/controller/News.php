<?php
/**
 * Created by PhpStorm.
 * User: wtfssd
 * Date: 2017/6/5
 * Time: 17:29
 */

namespace app\Api\controller;


use controller\BasicApi;
use think\Db;
use think\Response;

class News extends BasicApi
{
    public $table = 'wechat_news_article';

    public function index()
    {

    }

    /**
     * 根据分类别名获取文章列表
     */
    public function articleList()
    {
        if ($this->request->isGet()){
            $alias = $this->request->get('alias');
            if (empty($alias)){
                return $this->response("请求参数错误",203,[]);
            }
            $data=[];
            $db = Db::name($this->table)
                ->where('category',$alias)
                ->select();
            $data['articles'] =  array_map(function ($article){
                return [
                    'id'=>$article['id'],
                    'title'=>$article['title'],
                    'icon'=>$article['local_url'],
                    'brief'=>$article['digest'],
                ];
            },$db);
            $db=Db::name('wechat_categorys')
                ->where('alias',$alias)
                ->select();
            $data['category']=$db[0];
            return $this->response("ok",200,$data);
        }
        return $this->response("请求方法错误",204,[]);
    }


    /**
     * 获取文章详情
     */
    public function articleDetail(){
        if($this->request->isGet()){
            $id = $this->request->get('id');
            if (empty($id)||strlen($id)<=0){
                return $this->response("请求参数错误",203,[]);
            }
            $db=Db::name($this->table)
                ->where('id',$id)
                ->select();

            return $this->response("ok",200,$db[0]);
        }
        return $this->response("请求方法错误",204,[]);
    }
}
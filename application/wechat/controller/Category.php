<?php
/**
 * Created by PhpStorm.
 * User: wtfssd
 * Date: 2017/6/6
 * Time: 08:47
 */

namespace app\wechat\controller;


use controller\BasicAdmin;
use service\DataService;
use think\Db;
use think\Request;
use think\Response;


class Category extends BasicAdmin
{
    private $tableName = "WechatCategorys";


    public function __construct(Request $request = null)
    {

        parent::__construct($request);
    }

    public function Index()
    {

        $data = Db::name($this->tableName);

        $this->assign('title', '文章分类');


        return parent::_list($data);
    }

    public function add()
    {
        $errMsg ='';
        if ($this->request->isGet()) {
            return view('add', ['title' => "添加分类",'vo'=>[]]);
        } elseif ($this->request->isPost()) {
            $data = $this->request->post();
            if (($post=$this->_apply_category($data["data"]))&&!empty($post)) {
                if (strlen($post['alias'])>0){
                    if(DataService::save($this->tableName,$post,'id')!==false){
                        $this->success('分类添加成功','');
                    }else{
                        $errMsg="操作失败";
                    }
                }else{
                    $errMsg = "分类别名不能为空";
                }
            }
        }
        $this->error('图文添加失败，请稍候再试！'.PHP_EOL.$errMsg,'');
    }

    public function del(){
        $data = $this->request->post();
        $res = Db::name($this->tableName)->where('alias',$data['alias'])->delete();
        if ($res == true){
            $this->success('分类删除成功','');
        }
        $this->error('分类删除失败','');
    }

    public function edit(){
        $errMsg ='';
        if($this->request->isGet()){
            $data = $this->request->get();
            $db =Db::name($this->tableName)->where('alias',$data['alias'])->find();
            return view('add', ['title' => "编辑分类",'vo'=>$db]);
        }
        if ($this->request->isPatch()){
            $data = $this->request->patch();
            if (($patch=$this->_apply_category($data["data"]))&&!empty($patch)) {
               if (Db::name($this->tableName)->where('alias',$patch['alias'])->update([
                   'name'=>$patch['name'],
                   'icon'=>$patch['icon'],
                   'cover'=>$patch['cover'],
               ])){
                   $this->success('修改成功'.$errMsg,'');
               }else{
                   $errMsg='信息未改变';
               }
            }
        }
        $this->error('修改失败'.$errMsg,'');

    }

    protected function _apply_category($data, $ids = [])
    {

        $_data = urldecode($data);

        $temp = [];
        foreach (explode('&',$_data) as $value){
            $key = explode('=',$value)[0];
            $value = explode('=',$value)[1];
            $temp[$key] = $value;
        }
        $temp['create_at'] = date('Y-m-d H:i:s');
        return $temp;
    }



    public function categories(){
        $db = Db::name($this->tableName);
        return $db;
    }
}

